package no.noroff.accelerate.ocp.end;

import no.noroff.accelerate.isp.end.Logger;
import no.noroff.accelerate.isp.end.Reader;
import no.noroff.accelerate.srp.start.WeatherData;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

public abstract class WeatherStation {
    private List<WeatherData> readings = new ArrayList<>();
    private Reader reader;
    private Logger logger;
    private String name;

    public WeatherStation(Reader reader, Logger logger, String name)
    {
        this.reader = reader;
        this.logger = logger;
        this.name = name;
    }

    public void readWeatherData(String path) throws FileNotFoundException {
        readings = reader.read(path);
    }

    public void logWeatherData(WeatherData point) {
        logger.log(point);
    }
}
