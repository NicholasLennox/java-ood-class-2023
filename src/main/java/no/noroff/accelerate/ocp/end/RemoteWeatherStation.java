package no.noroff.accelerate.ocp.end;

import no.noroff.accelerate.isp.end.Logger;
import no.noroff.accelerate.isp.end.Reader;

import java.io.FileNotFoundException;

public class RemoteWeatherStation extends WeatherStation {
    private double latitude;
    private double longitude;

    public RemoteWeatherStation(String name, double latitude, double longitude, Logger logger, Reader reader) {
        super(reader, logger, name);
        this.latitude = latitude;
        this.longitude = longitude;
    }

    // Print/logging
    @Override
    public void readWeatherData(String path) throws FileNotFoundException {
        System.out.println("Logging into jadadada...");
        super.readWeatherData(path);
        System.out.println("Done reading data");
    }
}
