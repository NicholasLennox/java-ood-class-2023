package no.noroff.accelerate.ocp.end;

import no.noroff.accelerate.isp.end.Logger;
import no.noroff.accelerate.isp.end.Reader;

public class LocalWeatherStation extends WeatherStation {
    private String address;

    public LocalWeatherStation(String name, String address, Logger logger, Reader reader) {
        super(reader, logger, name);
        this.address = address;
    }
}
