package no.noroff.accelerate.lsp.start;

import no.noroff.accelerate.ocp.end.LocalWeatherStation;
import no.noroff.accelerate.ocp.end.RemoteWeatherStation;
import no.noroff.accelerate.ocp.end.WeatherStation;

import java.io.FileNotFoundException;
import java.util.Scanner;

public class WeatherStationClient {
    private WeatherStation station;

    public void setup() {

        System.out.println("What is the name of the station?: ");
        Scanner scanner = new Scanner(System.in);
        String stationName = scanner.nextLine();
        System.out.println("Is this a (l)ocal or (r)emote station?: ");
        String stationType = scanner.nextLine();
        if(stationType == "l")
        {
            System.out.println("What is the address?: ");
            String address = scanner.nextLine();
            //station = new LocalWeatherStation(stationName,address);
        } else if (stationType == "r")
        {
            // For the sake of simplicity, we are going to leave out input validation.
            System.out.println("What is the latitude?: ");
            var latitude = scanner.nextDouble();
            System.out.println("What is the longitude?: ");
            var longitude = scanner.nextDouble();
            //station = new RemoteWeatherStation(stationName, latitude, longitude);

        } else
        {
            System.out.println("Incorrect choice, try again");
            setup();
        }
    }

    public void readData(String path)
    {
        try {
            station.readWeatherData(path);
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
    }


}
