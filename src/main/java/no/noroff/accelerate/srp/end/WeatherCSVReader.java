package no.noroff.accelerate.srp.end;

import no.noroff.accelerate.isp.end.Reader;
import no.noroff.accelerate.isp.start.DataProcessor;
import no.noroff.accelerate.srp.start.WeatherData;

import java.io.FileNotFoundException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

public class WeatherCSVReader implements Reader {
    public List<WeatherData> read(String path) {
        if (!Files.exists(Paths.get(path)))
            try {
                throw new FileNotFoundException();
            } catch (FileNotFoundException e) {
                throw new RuntimeException(e);
            }
        // Real implementation of reading in data, we will fake it for demo
        // It would be a CSV file
        return Arrays.asList(
                new WeatherData(LocalDate.now(), 10, 50, 1000 ),
                new WeatherData(LocalDate.now().minusDays(1), 12, 60, 1010));
    }
}
