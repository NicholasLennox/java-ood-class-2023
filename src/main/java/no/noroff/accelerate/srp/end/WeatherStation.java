package no.noroff.accelerate.srp.end;

import no.noroff.accelerate.srp.start.WeatherData;

import java.io.FileNotFoundException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class WeatherStation {
    private List<WeatherData> readings = new ArrayList<>();
    private WeatherCSVReader reader = new WeatherCSVReader();
    private WeatherConsoleLogger weatherConsoleLogger = new WeatherConsoleLogger();
    private String name;

    public WeatherStation(String name)
    {
        this.name = name;
    }

    public void readWeatherData(String path) throws FileNotFoundException {
        readings = reader.read(path);
    }

    public void logWeatherData(WeatherData point) {
        weatherConsoleLogger.log(point);
    }
}
