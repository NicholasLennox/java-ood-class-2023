package no.noroff.accelerate.srp.end;

import no.noroff.accelerate.isp.end.Logger;
import no.noroff.accelerate.isp.start.DataProcessor;
import no.noroff.accelerate.srp.start.WeatherData;

import java.util.List;

public class WeatherConsoleLogger implements Logger {

    public void log(WeatherData data) {
        System.out.println("\nWEATHER LOG ---------------");
        System.out.println("Date:" + data.getDate());
        System.out.println("Temperature:" + data.getTemperature());
        System.out.println("Humidity:" + data.getHumidity());
        System.out.println("Pressure:" + data.getPressure());
        System.out.println("---------------------------\n");
    }
}
