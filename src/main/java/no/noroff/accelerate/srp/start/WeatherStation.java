package no.noroff.accelerate.srp.start;

import java.io.File;
import java.io.FileNotFoundException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class WeatherStation {
    protected List<WeatherData> readings = new ArrayList<>();
    private String name;

    public WeatherStation(String name)
    {
        this.name = name;
    }

    public void readWeatherData(String path) throws FileNotFoundException {

    }

    public void logWeatherData(WeatherData point)
    {

    }
}
