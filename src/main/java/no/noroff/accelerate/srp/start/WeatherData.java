package no.noroff.accelerate.srp.start;

import java.time.LocalDate;
import java.util.Objects;

public class WeatherData {
    private LocalDate date;
    private int temperature;
    private int humidity;
    private int pressure;
    public WeatherData(LocalDate date, int temperature, int humidity, int pressure) {
        this.date = date;
        this.temperature = temperature;
        this.humidity = humidity;
        this.pressure = pressure;
    }



    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public int getTemperature() {
        return temperature;
    }

    public void setTemperature(int temperature) {
        this.temperature = temperature;
    }

    public int getHumidity() {
        return humidity;
    }

    public void setHumidity(int humidity) {
        this.humidity = humidity;
    }

    public int getPressure() {
        return pressure;
    }

    public void setPressure(int pressure) {
        this.pressure = pressure;
    }
}
