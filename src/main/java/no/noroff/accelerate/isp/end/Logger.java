package no.noroff.accelerate.isp.end;

import no.noroff.accelerate.srp.start.WeatherData;

public interface Logger {
    void log(WeatherData point);
}
