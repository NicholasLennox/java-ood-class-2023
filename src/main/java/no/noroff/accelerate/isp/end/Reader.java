package no.noroff.accelerate.isp.end;

import no.noroff.accelerate.srp.start.WeatherData;

import java.util.List;

public interface Reader {
    List<WeatherData> read(String path);
}
