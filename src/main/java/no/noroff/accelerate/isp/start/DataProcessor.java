package no.noroff.accelerate.isp.start;

import no.noroff.accelerate.srp.start.WeatherData;

import java.util.List;

public interface DataProcessor {
    List<WeatherData> read(String path);
    void log(WeatherData point);
}
